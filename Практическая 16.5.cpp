﻿#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int sum = 0;

    const int size = 5; 

    int numStr = buf.tm_mday % size;
    
    int array[size][size];

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";               

            
        }
        std::cout << "\n";                               
    }

    //с использованием одного фор не получалось, пришлось вынести
    for (int j = 0; j < size; j++)
    {
        sum = sum + array[numStr][j];
    }

    std::cout << "day: " << buf.tm_mday << std::endl;
   
    std::cout << "index: " << numStr << std::endl;

    std::cout << "sum: " << sum << std::endl;
}

